SLIDE_FMT: new-topic

# Introducción

----

## Máquinas virtuales

* Abstraen el hardware de una máquina física en múltiples ambientes de ejecución
  diferentes: CPU, memoria, discos, interfaces de red y otros dispositivos.
* Cada ambiente genera la _ilusión_ de correr en un hardware dedicado.

----

## Componentes para implementar VMs

* En la base está el **host**, que es quien dispone del hardware que correrá las
  máquinas virtuales.
* El **hypervisor** o **Virtual Machine Manager** (VMM) creará y correrá máquinas
  virtuales proveyendo de una interfaz **_idéntica_** al **_host_**.
  * _Excepto en paravirtualización que se verá luego._
* Cada **guest** recibe una copia del _host virtual_. Los guest son las máquinas
  virtuales que corren un SO.

> Una máquina física puede correr múltiples SO diferentes en cada VM.

----

## Modelos de sistemas

![system models](images/system-models.png)<!-- .element: width="600px" -->
<div class="small" >

**(a)** Máquina física  / **(b)** Máquina física, VMM y guests
</div>

----

## El hypervisor

En virtualización, el concepto de SO se distorsiona. Algunos VMM como es el caso
de [VMWare ESX](https://www.vmware.com/products/esxi-and-esx.html):

* Se instala en el **host**.
* Bootea y gestiona aplicaciones.
* Provee servicios a las aplicaciones como los tradicionales en un SO de
  scheduling y gestión de memoria. Además, provee otros más específicos como los
  que permiten migrar VMs entre sistemas.

> ¿Es entonces el VMM VMWare ESX un SO que además corre y gestiona virtuales?
> Actúa como un SO, pero por claridad usaremos el término VMM / hyperivisor
> al componente que provee ambientes virtuales.

----
<!-- .slide: data-auto-animate --> 

## Implementaciones de hypervisores

La forma en que se implementan los VMM varían ampliamente según han ido
evolucionando. Estas diferencias se han tipificado de la siguiente forma:

* **Hipervisores de tipo 0:** soluciones basadas en hardware que permiten la
  creación y gestión de VMs a través del firmware. Es tecnología legada,
  utilizada principalmente en mainframes o servidores de gran escala.
----
<!-- .slide: data-auto-animate --> 

## Implementaciones de hypervisores

* **Hipervisores de tipo 1:** aparecen dos clasificaciones:
  * Software dedicado que fue creado exlusivamente para ofrecer ambientes
    de virtualización o VMM.
  * SO de propósito general como Windows o Linux que ofrecen capacidades de
    virtualización, es decir son VMM, y además un clásico SO.
* **Hipervisores de tipo 2:** también llamados **_hosted hypervisors_**, son
  _aplicaciones_ que corren sobre un SO estándar y confían en el SO preexistente
  para manejar los recursos virtuales. Esto introduce una **_latencia_**
  inevitable en relación a los casos anteriores.

----
<!-- .slide: data-auto-animate --> 

## Implementaciones de hypervisores

* **Paravirtualización:** técnica donde el SO guest debe modificarse para
  trabajar en cooperación con el VMM para optimizar su performance. Esto genera
  una alta cohesión entre guest y el VMM.
* **Ambientes virtualizados de programación:** el VMM no virtualiza hardware
  real sino que por el contrario crea un sistema virtual optimizado. La
  principal diferencia con los anteriores, es que se utiliza para correr
  aplicaciones y no SOs.
----
<!-- .slide: data-auto-animate --> 
## Implementaciones de hypervisores

* **Emuladores:** permite correr aplicaciones escritas para un hardware
  determinado en un ambiente completamente diferente, donde incluso la CPU
  podría ser de otra arquitectura.
* **Contenedores:** no sería adecuado llamarlos virtualización, aunque proveen
  características similares indendependizando las aplicaciones del SO
  subyacente. Proveen una forma amigable de empaquetar, distrubuir y ejecutar
  aplicaciones que autocontienen sus dependencias.
  * La unidad de ejecución es una aplicación, no un SO.
----

## Ejemplos concretos

* **Tipo 0:** [IBM LPARs](https://www.ibm.com/docs/en/zos-basic-skills?topic=design-mainframe-hardware-logical-partitions-lpars),
  [Oracle / Sun LDOMs](https://en.wikipedia.org/wiki/Oracle_VM_Server_for_SPARC).
* **Tipo 1:** [XenServer](https://www.xenserver.com/), [VMWare ESXi](https://www.vmware.com/products/esxi-and-esx.html),
  [kvm](https://linux-kvm.org/page/Main_Page), [Microsoft Hyper-V](https://en.wikipedia.org/wiki/Hyper-V).
* **Tipo 2:** [VirtualBox](https://www.virtualbox.org/),
  [VMWare Workstation](https://www.vmware.com/products/desktop-hypervisor.html).
* **Paravirtualización:** [Xen versiones anterioroes](https://wiki.xenproject.org/wiki/Paravirtualization_(PV)).
* **Ambientes de lenguajes de programación:** Java, .Net.
* **Emuladores:** [Hercules emulador de mainframe](http://www.hercules-390.org/), [MAME arcade / consolas retro de juegos](https://www.mamedev.org/),
  [PCSX2 Play station 2](https://pcsx2.net/), [Yuzu Nintendo switch](https://yuzu-emu.org/).
* **Contenedores:** [LXC](https://linuxcontainers.org/),
  [Docker](https://www.docker.com/), [Podman](https://podman.io/).

----
## Por qué tantas implementaciones

La cantidad de técnicas de virtualización utilizadas hoy día denotan la
amplitud, extensión e importancia de ella en los sistemas modernos. Hoy es un
concepto invaluable para la operatoria de data centers y el eficiente desarrollo
de aplicaciones.
