SLIDE_FMT: new-topic
# Beneficios y características
----
<!-- .slide: data-auto-animate -->

## Ventajas de la virtualización

* Compartir el mismo hardware para correr diferentes ambientes de ejecución
  (distintos SO) de manera concurrente (no con dual boot).
* El **host** está protegido de cada VM o guest, así como cada VM está protegida
  de las demás.
  * Una vulnerabilidad o virus en un guest no afectará otras.
* **Desarrollo o estudio de sistemas operativos:** permite realizar
  pruebas sin afectar al **host**.
----
<!-- .slide: data-auto-animate -->
## Ventajas de la virtualización

* **Durante el desarrollo de aplicaciones:**
  * Permitiendo verificar la portabilidad o incluso funcionamiento de un
    producto en nuevas versiones de SO.
  * Armando ambientes complejos utilizando VMs que corren servicios que son
    parte de arquitecturas grandes.
* **Al trabajar con [Infraestrcutura como código (IaC)](https://en.wikipedia.org/wiki/Infrastructure_as_code):**
  como en desarrollo, se recomienda testear los fuentes antes de aplicarlo. Es
  más se promueve la adopción de  de TDD. El código es probado en diferentes
  plataformas usando virtualización.
----
<!-- .slide: data-auto-animate -->
## Ventajas de la virtualización

* **Consolidación de sistemas físicos en virtuales:** la conversión de sistemas
  físicos en VMs ofrece una reducción de espacio y optimización de recursos.
  * Por ejemplo, un data center con 10 servidores físicos, cada uno
    corriendo 80 VMs, sin virtualización necesitaría 800 servidores.
  * En consecuencia, menor espacio, consumo energético y calórico promoviendo
    [Green computing](https://en.wikipedia.org/wiki/Green_computing).
* **Templates:** modelo de VM creado con una serie de personalizaciones listas
  para poder _**crear nuevas VMs a partir de él**_ con un menor esfuerzo de
  configuración. Por ejemplo, ya disponer de un proxy, paquetes y/o
  configuraciones de seguridad, etc.
----

## Desventajas

* Una desventaja del aislamiento de VMs es la forma en que **comparten recursos**
entre sí:
  * Surgen así dos formas de compartir recursos:
    * Usando filesystems compartidos y a través de ellos compartiendo
      archivos.
    * Usando una red de máquinas virtuales donde cada una de ellas se comunica. La
      red puede ser modelada como una abstracción sin salida a la red física o sí.
      Esto dependerá del VMM y su configuración.
* **Virtualization / VM sprawl:** fenómeno que se da cuando el número de
  VMs en una red crece al punto de volverse inmanejable.
----

## Ciclo de vida de las VMs

La mayoría de los VMMs manejan estados de sus VMs:

* **Stopped:** una VM que no se está ejecutando. Sería análogo a pensar que una
  máquina física se encuentra apagada.
* **Running:** una VM que se está ejecutando.
* **Suspended:** una VM suspendida es similar a poner una maquina en reposo o
  mejor dicho, hibernando. Al suspender la ejecución de una VM, no solamente se
  almacena su disco, sino el estado de la misma (memoria, registros, etc).

----

## Snapshots

El concepto de snapshot es el de tomar una _instantánea_ del storage de una VM
en un momento mientras continúa su ejecución. No hay demoras en el acto del
snapshot si se utilizan filesystems o volúmenes lógicos que así lo soporten (por
ejemplo LVM).

<div class="small">

Al poder freezar el filesystem de una virtual usando snapshots:

* Se puede migrar de un host físico a otro.
* Clonar una VM a partir de otra.
* Tomar un resguardo antes de realizar un cambio y así poder disponer de point
  in time recovery.
* Incluso aunque no es aconsejable, como política de backup.

</div>
----

## Live migration

Los VMMs no solamente mejoran el uso de recursos sino que además simplifican la
gestión de los mismos ofreciendo la capacidad de **live migration** que permite
mover VMs en estado **running** desde un servidor físico a otro sin interrumpir
su operación o conexiones de red que mantenga activas.

Suele usarse para liberar de carga a un **host** que se encuentre sobrecargado
sin interrumpir el servicio del **guest** que se migre.

También permite realizar mantenimiento sobre HW físico moviendo sus guests y
evacuandolo para su mantenimiento.

----

## Estándares portables

* En 2007 se estandariza el [Open Virtualization Format](https://en.wikipedia.org/wiki/Open_Virtualization_Format)
  u OVF, propuesto por VMWare, Dell, HP, IBM y XenSource.
* El formato consta de un conjunto de archivos ubicados en un directorio:
  * Un descriptor con extensión `.ovf` que no es más que un XML on descripciones
    de la VM empaquetada, como nombre, requerimietos de HW y referencias a otros
    archivos dentro del paquete OVF.
  * Imágenes de discos, certificados y otros archivos auxiliares
* El directorio completo se distribuye como un paquete [Open Virtual
  Appliance](https://en.wikipedia.org/wiki/Virtual_appliance) u OVA, que no es
  más que un tar con el contenido del directorio OVF.
----

## Otros usos de la virtualización

* Además de pensar en servicios en un data center tradicional, podemos mencionar
  el concepto de **Cloud Computing**. Las plataformas Cloud utilizan fuertemente
  a la virtualización para ofrecer sus servicios. Éstos se basan en APIs que
  permiten a los usuarios gestionar su infraestructura por demanda a un costo
  asociado al uso. Entre otras cosas, las APIs permiten cambiar la escala en
  base a costos o demanda.
* También es habitual **virtualizar los desktops** utilizando entonces en cada
  puesto de trabajo dispositivos más económicos que se conectan a un escritorio
  remoto. Podemos mencionar [RDP](https://en.wikipedia.org/wiki/Remote_Desktop_Protocol),
  [Spice](https://spice-space.org/),
[NX](https://en.wikipedia.org/wiki/NX_technology), [VNC](https://en.wikipedia.org/wiki/Virtual_Network_Computing) e incluso [devcontainers](https://containers.dev/).


