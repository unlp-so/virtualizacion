SLIDE_FMT: new-topic
# Historia
----
<!-- .slide: data-auto-animate -->
## Línea del tiempo

* IBM comienza a explorar la virtualización en mainframes en 1964.
* En 1972, IBM lanza [System/370](https://en.wikipedia.org/wiki/IBM_System/370),
  un sistema que ha continuado evolucionando hasta la actualidad. Sus conceptos
  aún se mantienen vigentes en otros sistemas.
  * Divide al mainframe en múltiples VMs, cada una corriendo su propio SO.
    Además introdujo el concepto de discos virtuales o _minidisks_.
* Por 1990, también IBM introduce el [System/390](https://en.wikipedia.org/wiki/IBM_System/390),
  capaz de soportar particionamiento lógico de la CPU física. Cada CPU podía
  dividirse en hasta 10 vCPU, y donde cada una es independiente.
----
<!-- .slide: data-auto-animate -->
## Línea del tiempo

* Recién en 1999, VMWare comienza a a diseñar sus primeros productos de
  virtualización en x86. _Inicia la era de virtualización en x86_.
* En el año 2002 se libera como software libre [Xen](https://xenproject.org/),
  que surge como un proyecto de investigación de la Universidad de Cambridge
  llamado [Xenoserver](https://www.cl.cam.ac.uk/research/srg/netos/projects/archive/xeno/).
  * Dado que el HW de ese momento no soportaba virtualización asistida por HW,
    es Xen quien introduce la **paravirtualización**.
  * A partir de Xen 1.0, se comenzó a incluir en distribuciones Linux.

----
<!-- .slide: data-auto-animate -->
## Línea del tiempo

* Entre 2005 y 2006 Intel (VT-x) y AMD (AMD-v) introducen un soporte limitado de
  [virtualización asistido por HW](https://en.wikipedia.org/wiki/X86_virtualization).
* Ya a fines de 2005 Xen lanza la versión 3.0 con soporte oficial de tecnología
  Intel VT-x y la [arquitectura IA64](https://en.wikipedia.org/wiki/IA-64). Esta
  versión soporta guests que no debían ser modificados como sucede con la
  paravirtualización.
* También en 2005, Hewlett Packard lanza [HP Integrity Virtual
  Machines](https://en.wikipedia.org/wiki/HP_Integrity_Virtual_Machines) para el
  Unix de HP, [HP-UX](https://en.wikipedia.org/wiki/HP-UX).

----
<!-- .slide: data-auto-animate -->
## Línea del tiempo

* Qumranet, una empresa israelí, comenzará un desarrollo que es liberado en 2006
  como el famoso [KVM](https://linux-kvm.org/) o Kernel Virtual Machine.
  * KVM a diferencia de otros VMM, no es un producto independiente sino que
    apoyado en el Kernel Linux utiliza módulos del SO para convertirlo en un
    hypervisor.
  * En octubre de 2006, los fuentes de KVM se integran al kernel linux.

----
<!-- .slide: data-auto-animate -->
## Línea del tiempo
* En 2008 Microsoft lanza [Hyper-V](https://en.wikipedia.org/wiki/Hyper-V) en
  Windows Server 2008 R2.
* En este mismo año [Redhat adquiere
  Qumranet](https://www.redhat.com/en/about/press-releases/qumranet) por u$s 100
  millones. Esto significó el reemplazo de Xen por KVM en los productos de RH.
* Mientras la virtualización estaba en su punto máximo, una tecnología liviana
  de virtualización estaba emergiendo lentamente: **los contenedores**. Basados
  en el concepto de chroot en Unix que data de 1979, en 2008 nace [LXC](https://linuxcontainers.org/).
  Implementados usando cgroups y linux namespaces y proveyendo interfaces de
  alto nivel para su integración con aplicaciones.
----
<!-- .slide: data-auto-animate -->
## Línea del tiempo

* Docker surge en 2013 como un proyecto open source para la completa gestión de
  contenedores: comandos por línea de comando y un servicio que se comunican a
  través de una API para gestionar el ciclo de vida completo de contenedores,
  desde su construcción, publicación en registries, un filesystem basado en capas
  y mucho más.
* En 2014 la empresa CoreOS lanza
  [rocket](https://www.redhat.com/en/topics/containers/what-is-rkt) como una
  alternativa superadora de docker en términos de seguridad.
* También en 2014, Google comienza el proyecto open source
  [Kubernetes](https://kubernetes.io/),
  siendo recién en 2015 el [lanzamiento de la versión 1.0](https://cloudplatform.googleblog.com/2015/07/Kubernetes-V1-Released.html).
  K8s un orquestador de contenedores de gran escala basado en [Borg](https://research.google/pubs/large-scale-cluster-management-at-google-with-borg/).
----
## Gráficamente

<div class="mermaid">
  <pre>
    %%{init: { }}}%%
    timeline
          1964: IBM empieza con virtualización en Mainframes
          1972: IBM System/370. Primer sistema con virtualización
          1990: IBM Systemm/390
          1979: Unix chroot
          1999: VMWare da los primeros pasos en virtualización en x86
          2002: Se libera Xen. Nace la paravirtualización
  </pre>
</div>

<div class="mermaid">
  <pre>
    %%{init: { }}}%%
    timeline
          2005: Intel VT-x, nace la virtualización asistida por HW en x86.
          2006: AMD-v sigue los pasos de Intel
          2005: HP lanza HP Integrity VMs sobre HP-Unix
          2006: Nace KVM dando soporte de VMM al kernel Linux
          2008: Microsoft lanza Hyper-v
              : Nace LXC
          2013: Docker
          2014: Rocket de CoreOS
              : Kubernetes  
  </pre>
</div>
