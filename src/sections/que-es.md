SLIDE_FMT: new-topic

# ¿Qué es?
----
## Definición

El término **virtualización** tiene múltiples significados en las ciencias de
computación. Pero todos coinciden en proveer recursos que tradicionalmente se
asocian a hardware pero en realidad son servicios que se comportan de igual
forma, es decir, _implementaciones_ que virtualizan o emulan, pero además
protegen, administran y limitan el uso del hardware.

> Claro está, que la palabra emular, no es adecuada en este contexto dado que,
> como veremos más adelante, es una clasificación de la virtualización.

----
<!-- .slide: dATA-Auto-animATE -->

## ¿Qué virtualizar?

Las máquinas virtuales son una instancia de la virtualización, tal vez la más
asociada al término. De hecho, en este curso nos adentraremos en ella. Sin
embargo, no es el único recurso virtualizable.
----
<!-- .slide: data-auto-animate -->

## ¿Qué virtualizar?

* **Storage:** las máquinas virtuales utilizan discos que si bien pueden ser
  dispositivos físicos, generalmente son **archivos** presentados
  virtualmente como dispositivos de bloque. En escenarios productivos, estos
  archivos son utilizados desde un storage externo aprovisionado desde una red
  de alta velocidad o [SAN](https://en.wikipedia.org/wiki/Storage_area_network).
* **Networking:** en este rubro, la virtualización se utiliza en
  [VPN](https://en.wikipedia.org/wiki/Virtual_private_network),
  [VLAN](https://en.wikipedia.org/wiki/VLAN), [redes overlay](https://en.wikipedia.org/wiki/Overlay_network)
  como ser [VxLAN](https://es.wikipedia.org/wiki/Virtual_Extensible_LAN).
  En los últimos tiempos emergen [NFV](https://en.wikipedia.org/wiki/Network_function_virtualization)
  y [SDN](https://en.wikipedia.org/wiki/Software-defined_networking) más en
  relación a la virtualización en data centers / cloud.

