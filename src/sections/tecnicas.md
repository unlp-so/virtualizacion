SLIDE_FMT: new-topic
# Técnicas de virtualización
----

## Introducción

* En este apartado veremos de qué forma se logra la virtualización en hypervisores
  de tipo 1 y 2. No aplican al tipo 0.
* La posibilidad de virtualizar depende de las características de la CPU del
  **host físico**. Si son suficientes será posible que exista un VMM que provea
  un ambiente a un **guest**. Caso contrario será imposible.
* Analizamos las técnicas de **trap and emulate** y **binary translation**, así
  como la importancia del soporte en el HW para su implementación.
----
<!-- .slide: data-auto-animate -->

## Problemas de virtualizar

* No todos los procesadores discriminan entre instrucciones privilegiadas y no
  privilegiadas: **como en las arquitecturas x86**.
* El artículo de 1974, [**Formal requirements for Virtualizable Third Generation Architectures**](https://en.wikipedia.org/wiki/Popek_and_Goldberg_virtualization_requirements)
  (_Popek & Goldberg_), indica que un VMM debe ofrecer:
  * **Fidelidad:** los programas que corren en una VM lo hacen igual que de forma nativa, esto es sin modificaciones.
  * **Performance:** una buena cantidad de instrucciones del guest se ejecutan por el HW sin intervención del VMM.
  * **Seguridad:** el VMM maneja el acceso al HW.
----
<!-- .slide: data-auto-animate -->

## Problemas de virtualizar

* El artículo además describe características que debe cumplir el [conjunto de
  instrucciones de la arquitectura](https://en.wikipedia.org/wiki/Instruction_set_architecture)
  (ISA) para implementar un VMM.
* Aplica sólo en arquitecturas de 3era generación (1964-1970) como el IBM/360,
  siendo difícil de extender a máquinas modernas.
* Enumera condiciones suficientes para garantizar virtualización, a partir de
  una clasificación de instrucciones ISA:
  * **Privilegiadas:** irrumpen cuando la CPU está en modo usuario. No si está
    en modo kernel.
  * **Sensibles:** intentan modificar la configuración de recursos del
    sistema directa o indirectamente: deshabilitar interrupciones, cambiar el
    timer, etc.
----
<!-- .slide: data-auto-animate -->

## Problemas de virtualizar

El teorema enuncia:

**_Cualquier computador de 3er generación podrá construir un VMM si el conjunto de
instrucciones sensibles es un subconjunto de las instrucciones privilegidas._**

> Esto significa que para construir un VMM es suficiente que todas las
> instrucciones que podrían afectar el funcionamiento del VMM (instrucciones
> sensibles y privilegiadas), siempre generen una interrupción que sea capturada
> por el VMM. Las instrucciones no privilegiadas pueden ejecutar nativamente.


----
## Virtual CPU o vCPU

* Un concepto importante en todo VMM es la implementación de una **virtual
  CPU**.
* No ejecutan código sino que representan el estado de la CPU como la VM
  **guest** cree que se encuentra.
* Para cada guest, el VMM mantiene una vCPU representando el estado actual de
  cada uno.
* Cuando el VMM debe cambiar el contexto de una VM a la CPU, la información de
  la VCPU es usada para cargar el contenido adecuado, como sucede con los
  procesos y el PCB ante un cambio de contexto.
----
<!-- .slide: data-auto-animate -->

## Trap and emulate

* Del artículo de Popek & Goldberg, surge esta técnica que también es conocida
  como _virtualización clásica_.
* Como ya sabemos, en sistemas dual mode, las instrucciones privilegiadas corren
  en modo kernel, mientras que las que no los son, en modo usuario.
* Las máquinas virtuales guest, al ser _aplicaciones_, deberían
  ejecutar únicamente en modo usuario.
* A su vez, la VM mantiene también modo dual y en consecuencia tendremos un
  virtual user mode y virtual kernel mode.
----
<!-- .slide: data-auto-animate -->

## Trap and emulate

* De igual forma que en el host nativo, una VM pasará de virtual user mode a
  virtual kernel mode.
* Cuando el kernel en el guest intente ejecutar alguna instrucción privilegiada
  o sensible, generará un trap porque el sistema real está en modo usuario:
  * El error es capturado por el VMM en el host, _**trap**_. Será el VMM quien
    gane el control y ejecute, _**emulate**_, la acción iniciada por el guest en
    su nombre.
  * Finalmente **_retornará_** el control a la VM guest.

> Puede apreciarse que las instrucciones privilegiadas entonces sufren una mínima
> latencia. No sucede esto con las instrucciones no privilegiadas que pueden
> correr nativamente en el HW, ofreciendo igual performance en un guest como
> nativamente.

----
<!-- .slide: data-auto-animate -->

## Trap and emulate

![trap and emulate](images/trap-and-emulate.png)<!-- .element: height="500px" -->
----

## La arquitectura x86

* Luego del auge de los mainframes, las computadoras de propósito general
  aparecen en el mercado hogareño.
* El propósito de estas computadoras nunca fue el de virtualizar.
* Recién en la década de 1990 se empezó a estudiar su posibilidad, con las
  limitante de no cumplir con las condiciones del teorema de Popek y Goldberg.
----

## Niveles de privilegio en x86

<div class="container">
<div class="col">

* En **ring 0** ejecuta el kernel del SO y drivers de dispositivos
* En **ring 3** las aplicaciones.
* Los rings 1 y 2 no son típicamente usados por el SO.

</div>
<div class="col">

![x86 rings](images/x86-rings.png)
</div>
</div>
----
<!-- .slide: data-auto-animate -->

## Instrucciones en x86

A continuación mostramos a modo de ejemplo algunas instrucciones categorizadas
según:

<div class="container small">
<div class="col">

#### Sensibles

* **SGDT:** Store GDT Register
* **SIDT:** Store IDT Register
* **SLDT:** Store LDT Register
* **SMSW:** Store Machine Status
</div>
<div class="col">

#### Privilegiadas

* **WRMSR:** Write MSR
* **CLTS:** Clear TS flag in CR0
* **LGDT:** Load GDT Register
* **INVLPG:** Flushes TLB entries for a page.

</div>
</div>

> El problema es que algunos instrucciones sensibles  **no son privilegiadas**
> como establece el teorema de Popek & Goldberg.
----
<!-- .slide: data-auto-animate -->
## Instrucciones en x86

ISA contiene aproximadamente 17 instrucciones sensibles que no son
privilegiadas:

```
SGDT, SIDT, SLDT, SMSW, PUSHF, POPF, LAR, LSL, VERR,
VERW, POP, PUSH, CALL, JMP, INT, RET, STR, MOV
```
----

## Ejemplo con instrucción POPF

```
PUSHF      # Mueve %EFLAGS al stack
ANDL $0x003FFDFF, (%ESP)  # AND para poner a 0 IF
POPF       # Mueve del stack to %EFLAGS
```
> Este código carga el [registro de
> flags](https://en.wikipedia.org/wiki/FLAGS_register) en el stack. La posición
> del stack está en el registro `ESP`, por ello, se hace un `ANDL` de 32 bits con
> una máscara que pone en 1 todos los bits salvo el 9 que es el IF (enmascarar
> interrupciones). Finalmente se carga nuevamente el registro `EFLAGS` con el
> bit modificado

----

# ¿Y el problema?

* Si la CPU está en el ring 0, entonces el bit IF es posible que se modifique
  con `POPF`.
* Pero si la CPU está en modo usuario, entonces `POPF` no modifica el bit IF.
  **Lo omite**.
  * Entonces no genera un trap esta acción porque `POPF` si bien es una
    instrucción sensible, no es considerada privilegiada.
  * Si no hay trap, el VMM no se enterará de este hecho cuando una VM intente
    ejecutar esta instrucción, que al ser un guest estatá en modo usuario.
* Por esto, **trap and emulate no funciona en x86**.
----
<!-- .slide: data-auto-animate -->

## Binary translation

Como solución al problema en x86, en 1998 VMware, introduce la técnica
de **binary translation**, simple de conceptualizar aunque difícil de
implementar.
----
<!-- .slide: data-auto-animate -->

## Binary translation

* Si la VCPU de un guest se encuentra en modo usuario, entonces podrá correr sus
  instrucciones de forma nativa en la CPU física.
* Cuando la VCPU del guest se encuentre en modo kernel, entonces el guest creerá
  que está corriendo en modo kernel.
  * El VMM examina cada instrucción que ejecute el guest en virtual kernel mode,
  adelantandose a las siguientes instrucciones que el guest ejecutará (basandose en el contador del programa
  de la VCPU).
  * Las instrucciones especiales serán traducidas a un nuevo conjunto
    de instrucciones que realicen una tarea similar.
----
<!-- .slide: data-auto-animate -->

## Binary translation

![binary translation](images/binary-translation.png)<!-- .element: height="500px" -->

----
<!-- .slide: data-auto-animate -->


## Binary translation

* La implementación más básica de BT explicada antes, funcionará bien pero su
  performance será muy mala.
* Si bien la mayoría de las instrucciones ejecutarán de forma nativa, las que no
  lo hacen agregarán una importante latencia porque **una única instrucción es
  reemplazada por varias**.
* Una mejora que introdujo VMWare es el uso de caché, donde el código de cada
  instrucción a ser traducido es reutilizado en vez de vuelto a generar.
----
<!-- .slide: data-auto-animate -->

## Manejo de la memoria

* Tanto en _Binary Translation_ como en _Trap & Emulate_, se utiliza **NPT**
(Nested Page Tables).
* Cada guest OS mantiene una o más tablas de paginación para traducir de memoria
  virtual a memoria física.
* El VMM mantiene NPTs que representan el estado de la tabla de paginación de los
  guests, de igual forma que lo hace con las vCPU.
* El VMM detecta cuando el guest intenta cambiar su tabla de paginación y realiza
  un cambio equivalente en el NPT.

----
<!-- .slide: data-auto-animate -->

## Manejo de la memoria

* Cuando un guest se encuentra ejecutando en la CPU, el VMM pondrá el puntero al
  NPT correspondiente en el registro apropiado de la CPU, de tal forma que la
  tabla sea la tabla de paginación activa.
* Cuando un guest necesita modificar la tabla de paginación (por ejemplo ante un
  fallo de página), la operación debe ser interceptada por el VMM y realizar las
  modificaciones necesarias en el NPT y las tablas de paginación del sistema.
* El uso de NPTs generará un incremento en las fallas del [TLB](https://en.wikipedia.org/wiki/Translation_lookaside_buffer),
  y muchas otras complicaciones que deberán manejarse para lograr un tiempo de
respuesta razonable.

----

## Virtualización asistida por HW en x86

* En 2005 Intel lanza una nueva generación de procesadores que ofrecen soporte
  nativo de virtualización proveyendo las instrucciones **VT-x**.
  * Con este soporte el uso de binary translation no fue más necesario.
* En 2006 AMD agregó el mismo soporte a través de la tecnologíam **AMD-V**.
 
> Ambos definen dos nuevos modos de operación: **host y guest**
----
<!-- .slide: data-auto-animate -->

### Cómo funciona la virtualización en x86

* El VMM ahora puede habilitar el **modo host**, definir las características de cada
máquinas virtual guest, y cambar el sistema al **modo guest**, pasándole el control
al SO guest que corre dentro de la virtual.
* En modo guest, el SO virtualizado creerá estar corriendo en hardware nativo y
verá aquellos dispositivos incluidos en la definición de la máquina virtual.
* Si el SO intenta acceder a un recurso virtualizado, el control es pasado al
  VMM para manejar esta interacción.
----
<!-- .slide: data-auto-animate -->

### Cómo funciona la virtualización en x86

* Otra mejora introducida asistida por HW es el manejo del direccionamiento de
  memoria a través de [SLAT](https://en.wikipedia.org/wiki/Second_Level_Address_Translation).
  * Es la implementación por HW del concepto de NPT.
  * Intel llamó a su implementación EPT (Extended Pages Tables).
  * AMD lo llamó RVI (Rapid Virtualization Indexing).

----

## Virtualización de dispositivos

* El acceso a dispositivos de I/O  es otro área que fue mejorada por la
  asistencia del HW.
* Si analizamos el controlador de acceso directo de memoria (DMA), éste recibe
  una memoria destino y un dispositivo I/O origen, y permite la transferencia
  directa entre ellos sin intervención del SO.
  * _¿Qué sucede si más de un guest intenta hacerlo?_

----

## IOMMU o I/O MMU Virtualization

* Los nuevos procesadores incluyen esta capacidad que permite a las máquinas
  virtuales guests utilizar directamente dispositivos físicos como ethernet,
  placas de video, controladores de discos, a través de DMA y remapeo de
  interrupciones.
* También suele llamarse esta tecnología como **PCI Passthrough**.
* AMD llama a esta tecnología **AMD-Vi**
* Intel la llama **VT-d**.
