SLIDE_FMT: main

# Virtualizacion
## Una introducción
----
## Objetivo

Este curso presenta el concepto de virtualización y sus diferentes
interpretaciones. Profundizaremos desde la perspectiva del Sistema Operativo,
repasando su evolución en el tiempo y las clasificaciones de los diferentes
hipervisores.

Al finalizar, se espera familiarizar al alumno con las terminologías
relacionadas, presentando además los productos mayormente utilizados hoy día.

----
## Agenda

* ¿Qué es la virtualización?
* Introducción
* Repaso histórico
* Beneficios y características
* Técnicas de virtualización
---

FILE: sections/que-es.md

---

FILE: sections/introduccion.md

---

FILE: sections/historia.md

---

FILE: sections/beneficios.md
---

FILE: sections/tecnicas.md

